﻿using Azure;
using Azure.AI.FormRecognizer.DocumentAnalysis;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FormRecognizer
{
    class Program
    {
        private static readonly string endpoint = "https://ocr-prod-bc.cognitiveservices.azure.com/";
        private static readonly string apiKey = "c98a0e0191c04d27990cc4186f59be7e";
        private static readonly string modelId = "bancorbras-inactive-v1";
        private static readonly string formUrl = "https://files.bomconsorcio.com.br/trans/ff7f4ae4-24db-46d2-892f-55d24034afe8.pdf";

        static void Main(string[] args)
        {
            var recognizerClient = AuthenticateClient();
            var analyzeForm = AnalyzePdfForm(recognizerClient, modelId, formUrl);
            Task.WaitAll(analyzeForm);
        }

        private static DocumentAnalysisClient AuthenticateClient()
        {
            AzureKeyCredential credential = new AzureKeyCredential(apiKey);
            DocumentAnalysisClient client = new DocumentAnalysisClient(new Uri(endpoint), credential);
            return client;
        }

        // Analyze PDF form data
        private static async Task AnalyzePdfForm(
            DocumentAnalysisClient client, String modelId, string formUrl)
        {
            Console.WriteLine("INICIANDO EXTRAÇÃO...");
            Console.WriteLine($"Documento: {formUrl} \n");

            Uri fileUri = new Uri(formUrl);
            AnalyzeDocumentOperation operation = await client.AnalyzeDocumentFromUriAsync(WaitUntil.Completed, modelId, fileUri);
            AnalyzeResult result = operation.Value;

            Console.WriteLine($"Document was analyzed with model with ID: {result.ModelId}");

            foreach (AnalyzedDocument document in result.Documents)
            {
                Console.WriteLine($"Document of type: {document.GetType()}");

                foreach (KeyValuePair<string, DocumentField> fieldKvp in document.Fields)
                {
                    string fieldName = fieldKvp.Key;
                    DocumentField field = fieldKvp.Value;

                    Console.WriteLine($"Field '{fieldName}': ");

                    Console.WriteLine($"  Content: '{field.Content}'");
                    Console.WriteLine($"  Confidence: '{field.Confidence}'");
                }
            }
        }
    }
}
