# Projeto Piloto: OCR Microsoft #

Este projeto representa um experimento da utilização do serviço de machine learning da Microsoft chamado Form Recognizer (https://azure.microsoft.com/en-us/services/form-recognizer/) para realizar a extração automática de informações dos extratos de cota.

Como escopo deste experimento, foi treinado um modelo com 14 extratos de cota da Magalu, e o código fonte do projeto executa o processo de reconhecimento com outras cotas a partir do modelo treinado.

O passo-a-passo para criação do ambiente e utilização do código-fonte são descritos abaixo.

Toda a documentação oficial do Form Recognizer pode ser encontrada em:

https://docs.microsoft.com/en-us/azure/applied-ai-services/form-recognizer/

## 1. Criar e configurar serviços da conta Azure

Primeiro, deve ser criada uma conta na Azure (https://portal.azure.com/). Após criada a conta, é necessário instanciar três serviços:

* Resource Group: serviço para organização dos recursos a serem utilizados (https://portal.azure.com/#blade/HubsExtension/BrowseResourceGroups)

* Storage: necessário a criação de uma Storage account e container para armazenar os PDFs dos extratos utilizados no treinamento do modelo (https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.Storage%2FStorageAccounts)

* Form Recognizer Service: serviço para treinamento e utilização do modelo de machine learning

## 2. Treinar modelo

O primeiro passo para treinar o modelo é fazer o upload dos arquivos para o Storage recém criado.

Para treinar o modelo, é necessário acessar uma ferramenta web da Microsoft (acesse pelo link https://aka.ms/from-recognizer/sample-tool) onde serão tagueados os elementos do documento a terem suas informações extraídas.

Na sua tela inicial, será configurada a ferramenta, indicando de onde os documentos serão baixados (do Blob storage com os arquivos que você colocou), e também informando a URL do seu serviço Form Recognizer junto com a chave de acesso para a API do mesmo.

Ao finalizar a configuração, e tudo ocorrendo adequadamente, será aberta uma interface com todos os documentos onde o treinamento será realizado.

### A. Criar tags e marcar documentos

O primeiro passo é criar as tags. Elas são os nomes de referência que representam as informações a serem extraídas. Para o experimento foram criadas as seguintes tags que desejamos extrair: Grupo, Cota, Documento, Nome, Telefone, CartaCredito, FundoComumPago, e SaldoDevedor.

Depois disso, você deverá abrir cada documento, clicar na região onde está a informação desejada, e em seguinda clicar na tag a qual ela pertence. Depois de associar todos os dados do documento às suas respectivas tags, você poderá passar para o próximo documento até concluir o conjunto de treinamento.

![Criação de tags e marcação nos documentos](/images/1.png "Processo de tagueamento")

### B. Treinar o modelo

Uma vez que todos os documentos foram marcados, basta escolher um nome para o modelo e selecionar a opção de treinar. Depois que o processo de treinamento for concluído, aparecerá as informações para utilização do modelo conforme imagem abaixo. Além disso, o modelo já apresenta os resultados de precisão que o modelo pode atingir.

![Resultado do treinamento](/images/2.png "Resultado do treinamento dos extratos de cota Magalu")

Exemplo dos dados de um modelo:

```
Model information

Model ID:
a6aee90d-7e4c-4637-988b-a571e2e9f04e

Model Name:
extrato-magalu

Created date and time:
1/27/2022, 4:00:49 PM

Average accuracy:
92.90%
```

## 3. Extrair dados dos documentos

O projeto de código-fonte criado no Visual Studio que está localizado na pasta code é responsável por aplicar o modelo treinado a um documento e retornar as informações extraídas.

### Recomendações para o projeto de desenvolvimento

* Instalar o pacote NuGet Azure.AI.FormRecognizer

* Tomar como base o código de exemplo disponível na classe Program do projeto FormRecognizer

